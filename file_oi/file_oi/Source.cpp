#include<iostream>
#include<fstream>
#include<string>

std::string filename = "CPP_Reference.txt"; // creates file name
std::string word; // string word
int wordNum; // int for number of words
int reserveNum; // int for reserved words
std::fstream file; // file
std::ofstream fileObj(" CPP_Enhanced_Reference.txt"); // file object

void FileWrite();
void FileRead();

void main()
{
	std::cout << "This is the C++ Memorizer\n";
	std::cout << "This will remember all the C++ references that you will need\n";
	std::cout << "Whenever you are ready ";
	system("pause"); // pauses the program
	system("CLS"); // clears the screen

	try
	{
		FileRead(); // runs function
		FileWrite(); // runs function
	}
	catch (...)
	{
		std::cout << "how did you get here";
	}

	std::cout << "\nThese References have been saved to another file\n";
	std::cout << "Thank you so much\nGoodBye!\n";
}

void FileRead()
{
	file.open(filename.c_str()); // opens file
	
	while (std::getline(file, word)) // checks every line of the file
	{
		if (wordNum < 63) // checks for the number to be less than 63
		{
			wordNum++; // adds 1
		}
		
		if(wordNum == 63) // checks if the number is 63
		{
			reserveNum++; // adds 1
		}
	}
	reserveNum--; // subtracts 1
}

void FileWrite()
{
	file.clear(); // clears files
	file.close(); // closes file
	file.open(filename.c_str()); // reopens the file
	
	std::cout << "C++ Keywords Defined = " << wordNum << "\n"; 
	std::cout << "C++ Keywords Defined = " << reserveNum << "\n\n";
	
	fileObj << "\nC++ Keywords Defined = " << wordNum << "\n" << std::endl; // prints this to the file
	
	while (std::getline(file, word)) // reads through the whole 
	{
		fileObj << word << std::endl; // puts all the lines in
		if(word == "While - looping construct") // checks for the last regular reference
		{
			fileObj << "\nC++ Reserved Defined = " << reserveNum << "\n" << std::endl; // prints this to the screen
		}
		std::cout << word << "\n";
	}
	
	fileObj.close(); // closes
}
	
	